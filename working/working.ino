/*

   Typical pin layout used:
   -----------------------------------------------------------------------------------------
               MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
               Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
   Signal      Pin          Pin           Pin       Pin        Pin              Pin
   -----------------------------------------------------------------------------------------
   RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
   SPI SS      SDA(SS)      10            53        D10        10               10
   SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
   SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
   SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/
#include <DS3231.h> 
#include <EEPROM.h>
#include <SPI.h>
#include <MFRC522.h>
#include "SevSeg.h"
#include "RF24.h"
#define RST_PIN         22          // Configurable, see typical pin layout above
#define SS_PIN          53         // Configurable, see typical pin layout above
#define BUZZER_TIME 50 //micro seconds
#define LED_SECONDS_THRESHOLD 8
#define LED_MINUTES_THRESHOLD 0
#define BUZZER_SECONDS_THRESHOLD 4
#define BUZZER_MINUTES_THRESHOLD 0
#define BUZZER_PIN 24
#define SECOND_RIDE_THRESHOLD 1
#define FIRST_RIDE_THRESHOLD 0
#define releu_pin A8

////////Modify here to add new car
#define CAR_ID 2

///////
#define nr_start_cards 3
#define nr_stop_cards 3
int real_month = 3;
int real_day = 10;
int real_hour = 1;
int real_minutes = 30;
int real_seconds = 0;
int current_laps=0;
int started_by;
struct backup_struct
{
 
  byte backup_minutes;
  byte backup_seconds;
  byte  backup_started;
  byte backup_employee;

};

///////////////////////////SECURITY ACCESS DATA


byte employee_key[nr_start_cards][4] = {{0xD0, 0xD8, 0xFB, 0x79},{0xF0, 0xA4,0xF8 ,0x79},{0x40, 0x7B, 0xF8, 0x79}};
byte stop_key[nr_stop_cards][4] = {{0x30, 0xC8, 0xF5, 0x79},{0xE0 ,0x5A, 0xFD,0x79},{0x50, 0xB1, 0xF9, 0x79}};



RF24 radio(9,8);
byte addresses[][6] = {"ABCDE"};
struct number_of_logs_struct
{
  word logs_number;
};

MFRC522 mfrc522(SS_PIN, RST_PIN);  // Create MFRC522 instance FOR rfid
boolean toggle1 = 0;
boolean started = 0;
int time_passed = 0;
int current_time = 0;
int time_passed_2 = 0;
boolean display_on = 0;
int blink_started = 0;
boolean clock_running = 0;
int lap_seconds = 0;
int lap_minutes = 0;
SevSeg sevseg;
boolean toggle2 = 0;
boolean toggle0 = 0;


void restore_clock()
{
  backup_struct aux;
  EEPROM.get(0, aux);
   started=aux.backup_started;
    
 if (started==1)
  {
    display_on=1;
   start_clock();
   started_by=aux.backup_employee;
    lap_seconds = aux.backup_seconds;
    lap_minutes = aux.backup_minutes;   
  }
}
void get_laps()
{
   number_of_logs_struct logs;
  EEPROM.get(sizeof(backup_struct), logs);
  current_laps=logs.logs_number;
}
void setup() {

pinMode(releu_pin,OUTPUT);
 get_laps();
  pinMode(8, OUTPUT);
  // put your setup code here, to run once:
  byte numDigits = 4;
  byte digitPins[] = {A0, A1, A2, A3};
  byte segmentPins[] = {A4, A5, 2, 3, 4, 5, 6, 7};
  sevseg.begin(COMMON_ANODE, numDigits, digitPins, segmentPins);
  sevseg.setNumber(8888, 0);
  Serial.begin(115200);		// Initialize serial communications with the PC
  while (!Serial);		// Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
  SPI.begin();			// Init SPI bus
  mfrc522.PCD_Init();		// Init MFRC522
  mfrc522.PCD_DumpVersionToSerial();	// Show details of PCD - MFRC522 Card Reader details
  Serial.println(F("Scan PICC to see UID, SAK, type, and data blocks..."));
  radio.begin();
 
   //rtc.begin();//start the clock
//////////////////////////Uncomment those lines to set the clock//////
  // rtc.setDOW(MONDAY);    
 // rtc.setTime(19, 27, 40);    
  //rtc.setDate(20, 3, 2017);  

 ///////////////////////////////////////////////////////////////
  radio.openWritingPipe(addresses[0]);//create pipe for radio transmission
  restore_clock();//restore the running lap


//Interrupt code
  cli();//stop interrupts
  //set timer1 interrupt at 1Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A = 124;//15624;// = (16*10^6) / (1*1024) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 and CS10 bits for 1024 prescaler
  TCCR1B |= (1 << CS02);//               use (1 << CS01)| (1 << CS00) for 64 prescaler
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);
  //
  //
  //
  //set timer0 interrupt at 2kHz
  //  TCCR0A = 0;// set entire TCCR2A register to 0
  //  TCCR0B = 0;// same for TCCR2B
  //  TCNT0  = 0;//initialize counter value to 0
  //   //set compare match register for 2khz increments
  //  OCR0A = 124;// = (16*10^6) / (2000*64) - 1 (must be <256)
  //   //turn on CTC mode
  //  TCCR0A |= (1 << WGM01);
  //   //Set CS01 and CS00 bits for 64 prescaler
  //  TCCR0B |= (1 << CS01) | (1 << CS00);
  //   //enable timer compare interrupt
  //  TIMSK0 |= (1 << OCIE0A);https://9gag.com/gag/a4b5w46
  sei();


  Serial.println("Arduino has completely started");

}
//ISR(TIMER0_COMPA_vect){//timer0 interrupt 2kHz toggles pin 8
////generates pulse wave of frequency 2kHz/2 = 1kHz (takes two cycles for full wave- toggle high then toggle low)
//  if (toggle0){
//
//    toggle0 = 0;
//
//
//  }
//  else{
//
//    toggle0 = 1;
//  }
//
//}

void start_clock()
{
  if (clock_running == 0)   //set the boolean that the clock is running and reset the
  {
    clock_running = 1;
    lap_minutes = 0;
    lap_seconds = 0;


  }
}
void stop_clock()
{
  real_seconds += lap_seconds;
  real_minutes += lap_minutes;
  
  clock_running = 0;
  lap_seconds = 0;
  lap_minutes = 0;
  display_on = 0;
  blink_started = 0;



}


void update_time()
{
  if (lap_seconds == 60)
  {
    lap_minutes++;
    lap_seconds = 0;
  }

}



void backup_clock()
{
  backup_struct aux;
   aux.backup_started=started;
  aux.backup_minutes = lap_minutes;
  aux.backup_seconds = lap_seconds;
  aux.backup_employee=started_by;
  EEPROM.put(0, aux);

}
void second_passed()
{


  if (clock_running)
  {
    lap_seconds++;
    update_time();
  }
  else
  {
    real_seconds++;
  
  }
  if (real_seconds == 0 || real_seconds == 30)
  {
    backup_clock();

  }

  sevseg.setNumber(lap_minutes * 100 + lap_seconds, 2);


}
//
ISR(TIMER1_COMPA_vect) { //timer1 interrupt 1Hz toggles pin 13 (LED)
  //generates pulse wave of frequency 1Hz/2 = 0.5kHz (takes two cycles for full wave- toggle high then toggle low)
  if (toggle1)

  {
    toggle1 = 0;
    if (display_on) {
      sevseg.refreshDisplay();
    }
   
  }
  else
  {

    toggle1 = 1;

  }

}




int sounds = 0;
void led_buzzer()
{
  if (lap_seconds >= LED_SECONDS_THRESHOLD && lap_minutes >= LED_MINUTES_THRESHOLD)
  {
    blink_started = 1;

  }

  if (lap_seconds >= BUZZER_SECONDS_THRESHOLD && lap_minutes >= BUZZER_MINUTES_THRESHOLD )
  {

    if (lap_seconds % 2 == 0 )
    {
      tone(BUZZER_PIN, 200, 15);
      delay(50);
      tone(BUZZER_PIN, 200, 15);

    }
  }
}
struct Usage_record {
  byte start_day;
  byte start_hour;
  byte start_minute;
  byte start_second;
  byte duration;
};
Usage_record record_to_save;

void update_record()      //save when the ride starts
{
  record_to_save.start_day = real_day;
  record_to_save.start_hour = real_hour;
  record_to_save.start_minute = real_minutes;
  record_to_save.start_second = real_seconds;


}
void key_accepted()
{
  started = 1;
    
   
    
    start_clock();
   backup_clock();
    display_on = 1;
  
}
void rfi_search()
{
  //scan new card and compare with existing keys
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return;
  }
  if (started == 0 )
  {
    for(int i=0;i<nr_start_cards;i++){
    if
  
  (
      mfrc522.uid.uidByte[0] == employee_key[i][0] &&
      mfrc522.uid.uidByte[1] == employee_key[i][1] &&
      mfrc522.uid.uidByte[2] == employee_key[i][2] &&
      mfrc522.uid.uidByte[3] == employee_key[i][3])
          {
              key_accepted();
             started_by=i+1;
         }
    }
  }
  
  if (started == 1)
  {
    for(int i=0;i<nr_stop_cards;i++)
    {
      if(
      mfrc522.uid.uidByte[0] == stop_key[i][0] &&
      mfrc522.uid.uidByte[1] == stop_key[i][1] &&
      mfrc522.uid.uidByte[2] == stop_key[i][2] &&
      mfrc522.uid.uidByte[3] == stop_key[i][3])
  {
     save_ride();
    started = 0;
      sevseg.blank();
    stop_clock();
    backup_clock();
    
  }
  }
  }
  //mfrc522.PICC_DumpToSerial(&(mfrc522.uid));



}
void save_ride()
{
  int increase_count=0;
  if(lap_minutes>=FIRST_RIDE_THRESHOLD)
  {
    increase_count++;
  }
  if(lap_minutes>=SECOND_RIDE_THRESHOLD)
  {
    increase_count++;
  }
  //record_to_save.duration = lap_minutes * 60 + lap_seconds;

  //add only the duration before saving
 number_of_logs_struct aux;
  EEPROM.get(sizeof(backup_struct), aux);
 // long ocuppied_space = (aux.logs_number * sizeof(Usage_record) + sizeof(backup_struct) + sizeof(number_of_logs_struct));
  //if (ocuppied_space > 1024)
//  {
//    aux.logs_number = 0;
//  }


  //EEPROM.put(sizeof(backup_struct) + sizeof(number_of_logs_struct) + aux.logs_number * sizeof(Usage_record), record_to_save);
  aux.logs_number += increase_count;
  current_laps+=increase_count;
  EEPROM.put(sizeof(backup_struct), aux);

  //send the employee who started it
      int status_length=4;
         int status[status_length];
         status[0]=1;//employee operation
      status[1]=CAR_ID;//car ID
      status[2]=started_by;//employee id
      status[3]=increase_count;//
      Serial.println(status[1]);
      Serial.println(status[2]);
      Serial.println(status[3]);
      Serial.println(status[4]);
      radio.write(status,sizeof(int)*status_length);
}
void print_logs()
{
  number_of_logs_struct logs;
  EEPROM.get(sizeof(backup_struct), logs);
  Serial.print("The number of rides is:");
  Serial.println(logs.logs_number);
  //for (int i = 0; i < logs.logs_number; i++)
//
//  {
//    Usage_record aux;
//    EEPROM.get(sizeof(backup_struct) + sizeof(number_of_logs_struct) + i * sizeof(Usage_record), aux);
//    Serial.println();
//    Serial.print("Log number:");
//    Serial.print(i);
//    Serial.print("   ");
//    Serial.print("day:");
//    Serial.print(aux.start_day);
//    Serial.print("   ");
//    Serial.print("hour:");
//    Serial.print(aux.start_hour);
//    Serial.print("   ");
//    Serial.print("minutes:");
//    Serial.print(aux.start_minute);
//    Serial.print("   ");
//    Serial.print("second:");
//    Serial.print(aux.start_second);
//    Serial.print("   ");
//    Serial.print("duration:");
//    Serial.print(aux.duration);
//
//
//
//
//  }
//  Serial.println();
  Serial.println("///////END LOGS////////////");
}
void reset_nr_laps()
{
  number_of_logs_struct aux;
  aux.logs_number=0;
  EEPROM.put(sizeof(backup_struct),aux);
}
unsigned long timer_30=0;
unsigned long current_time2=0;

///////////////////////////main method/////////////


void loop() {
           
    digitalWrite(A8,started?LOW:HIGH);
    
 
 current_time = millis();
  if (Serial.available())
  {
    
    char c = Serial.read();
    if (c == 'P')
    {
      print_logs();
    } else if (c == 'Z')
    {
      reset_nr_laps();
    }
  }
  
  current_time2=millis();
  if((current_time2-timer_30)>20000)
  {
    timer_30=current_time2;
    if(started==1)
    {
   
     backup_clock();
    }  
    else
    {
      int status_length=4;
         int status[status_length];
         status[0]=0;//operation code
      status[1]=CAR_ID;//car ID
      status[2]=current_laps;
      status[3]=10;//random value
      radio.write(status,sizeof(int)*status_length);
      
    }
  }
 if(blink_started)
    
    {
      if(display_on)
            
            {
                if( current_time - time_passed_2 > 1000)  ///sta pornit 1000 mili secunde
                  {
                          display_on=0;
                            time_passed_2 = current_time;
                    }
              
            }
            else
              {
                if(current_time-time_passed_2>300)   //sta oprit 300 mili secunde
                    {
                         display_on=1;
                         time_passed_2 = current_time;
                    }                             
                    }
              }
              
            
  }
  if (current_time - time_passed_2 > 500)
  {
    if (blink_started)        //UNCOMMENT FOR THE DISPLAY TO BLINK
   {
      display_on = !display_on;
   }
    time_passed_2 = current_time;
                         //  !!!!!!!!!!!!!!!!!ACTIVATE IT AGAIN WHEN STARTING
  }

  if (current_time - time_passed > 999)
  {
    time_passed = current_time;
    second_passed();
    led_buzzer();
  }
  //check if leds need to blink or the buzzer to make noise
  // Look for new cards
  rfi_search();


}
