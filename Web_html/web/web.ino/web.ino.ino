#include <Ethernet.h>
#include<SPI.h>
#include <EEPROM.h>
#include <DS3231.h>
#include "RF24.h"
#define nr_cars 9
#define message_length 4    //message erceived from child
#define nr_employees 4
#define SDA_CLOCK A9
#define SCL_CLOCK A10
#define months_period 4
     ///////////STARTS AT 1 !!!!!!!!!!!!!!

EthernetServer server = EthernetServer(12345);
RF24 radio(9, 53);

//IPAddress ip(192, 168, 1, 177);
DS3231  rtc(SDA_CLOCK, SCL_CLOCK);//THE CLOCK OBJECT
int current_month=0;

struct table      //the work for the fixed number of months months
{
  byte work[nr_cars ][nr_employees][months_period][31];
};
byte month_table[13]={-1,0,1,2,3,0,1,2,3,0,1,2,3};
table employee_table;  
void reset_records()
{

  for (int i = 0; i < nr_cars; i++)
  {
    for (int j = 0; j < nr_employees; j++)
    {
      for (int k = 0; k < months_period; k++)
      {
        for(int u=0; u<31; u++)
          {
            employee_table.work[i][j][k][u] = 0;
             // Serial.print("Finished");
          }
      }
    }
  }
  EEPROM.put(sizeof(current_month), employee_table);//clear table
  EEPROM.put(0,get_month());  //put current month

}


void restore_table()      
{
  //struct table table;//restore the work and month
  EEPROM.get(sizeof(current_month), employee_table);
  EEPROM.get(0,current_month);
// /
}

int get_month()//return the month as int
{
  char month[100];


  if (strcmp(rtc.getMonthStr(), "January") == 0)

    return 1;
  else if (strcmp(rtc.getMonthStr(), "February") == 0)


    return 2;

  else if (strcmp(rtc.getMonthStr(), "March") == 0)

    return 3;

  else if (strcmp(rtc.getMonthStr(), "April") == 0)

    return 4;
  if (strcmp(rtc.getMonthStr(), "May") == 0)


    return 5;
  if (strcmp(rtc.getMonthStr(), "June") == 0)

    return 6;
  if (strcmp(rtc.getMonthStr(), "July") == 0)

    return 7;
  if (strcmp(rtc.getMonthStr(), "August") == 0)

    return 8;
  if (strcmp(rtc.getMonthStr(), "September") == 0)

    return 9;
  if (strcmp(rtc.getMonthStr(), "October") == 0)

    return 10;
  if (strcmp(rtc.getMonthStr(), "November") == 0)

    return 11;
  if (strcmp(rtc.getMonthStr(), "December") == 0)

    return 12;


}
void backup_month() //store the current month
{
  int current_month=get_month();
  EEPROM.put(0,current_month);
}
void check_reset_month()//check if new month for reset
{
  int last_saved=0;
  current_month=get_month();  
  EEPROM.get(0,last_saved);     //get the last saved month
  if(last_saved!=current_month) //if a month passed
  {
   for (int i = 0; i < nr_cars; i++)
  {
    for (int j = 0; j < nr_employees; j++)
    {
        for(int u=0;u<31;u++)
          {
            
           employee_table.work[i][j][month_table[current_month]][u] = 0; //reset the new month
          }
      
    }
  }
  }
  create_backup();  //backup the new data
  backup_month();
  
}
void setup() {
  Serial.begin(115200);
  byte addresses[][6] = {"ABCDE"};
  byte mac[] = {0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02};
  radio.begin();
  rtc.begin();
 // Serial.print("This is the month");
 // Serial.print(get_month());
    rtc.setDate(4, 4, 2017);
  //reset_records();
  restore_table();
  check_reset_month;
  
  boolean receiving = false;

  if (Ethernet.begin(mac) == 0) {    Serial.println("Failed to Configure");   //configure the server
    return;
  }
  else {
    Serial.print("LocalIP address of the arduino:");
    Serial.println(Ethernet.localIP());
  }
 
  server.begin();
  Serial.println("Server Started");
  radio.openReadingPipe(1, addresses[0]);//open radio buffer
  radio.startListening();
  
}




void create_backup()
{
//  struct table table;
//  for (int i = 0; i < nr_cars; i++)
//  {
//    for (int j = 0; j <nr_employees; j++)
//    {
//
//      for (int k = 0; k < 12; k++)
//      {
//        for(int u=0 ;u<31;u++)
//        {
//          table.work[i][j][k][u] = employee_table[i][j][k][u];
//        }
//      }
//    }
//  }
  backup_month();
  EEPROM.put(sizeof(current_month), employee_table);

}

int get_day()
{
  Time t;
  t= rtc.getTime();
 return t.date;
  
}

byte records[nr_cars + 1];
void print_work()
{


  for (int i = 0; i <months_period; i++)
  {
    Serial.print("Month ");
    Serial.println(i+1);
    for(int u=0;u<31;u++)
        {
          Serial.print("    ");
          Serial.print("Day ");
          Serial.println(u+1);
          
        
    for (int j = 0; j <nr_employees ; j++)
    {
      Serial.print("         ");
      Serial.print("Employee ");
      Serial.println(j+1);
      for (int k = 0; k < nr_cars; k++)
      {
        Serial.print("                 ");
        Serial.print("Car ");
        Serial.print(k+1);
        Serial.print(": ");
        Serial.println(  employee_table.work[k][j][i][u]);
      }
    }
  }
  }
}


////////////////////////////////////////////////MAIN
void loop() {
  int updated_month=get_month();
  if (updated_month!=current_month)//check if month passed
  {
    check_reset_month();
  }
 
  char paskey[20] = "12345678";           ////////////This is the server's password
  //Serial.println(rtc.getMonthStr());
  if (Serial.available())
  {
    char c;
    c = Serial.read();
    if (c == 'P')
    {
      print_work();

    }
    else
    if(c =='R')
    {
      reset_records();
      restore_table();
    }
    else 
    if(c=='M')
    {
     // Serial.println(current_month);
       employee_table.work[8][3][3][30] +=2;

     // Serial.println(get_month()%(months_period)-1);
      //Serial.println(get_day()-1);
      create_backup();
    }
  }
  //Serial.println("Waiting for a client to connect");
  EthernetClient client = server.available();

  if (client) {
      Serial.print("Connected to a client : ");
      while (client.available()) {



      if (client.connected()) {
        char c = 'y';
        Serial.print("starting to print");
        char buf[100] = "";
        int reading = 0;
        int index = 0;
        while ((c =  client.read()) != ' ')
        {  }
        while ( (c =  client.read()) != ' ')
        {
          if (reading)
          {
            buf[index] = c;
            index++;

          }
          if ((c == '?') && (!reading))//if ? found in URL start reading password
          {
            reading = 1;
          }
          Serial.println();
        }
        if (strcmp(paskey, buf) == 0)//if password correct display tables
        {
          Serial.println("Response Sent to Client: A HTML Page");
          client.println("HTTP/1.1 200 OK");
          
//          client.println("Content-Type: text/html\n");
//          client.println("<table  border=1 cellpadding=5 cellspacing=5 style=width:100%>");
//          client.println("<tr><th colspan=12>Sumar Folosinta Masinute</th></tr>");
//          client.println("<tr><th rowspan=2></th><th>Valoare Tura</th><th colspan=2>Angajat 1</th><th colspan=2>Angajat 2</th><th colspan=2>Angajat 3</th></tr>");
//          client.println("<tr><td>(LEI)</td><td>Nr. ture</td><td>Val lei</td><td>Nr. ture</td><td>Val lei</td><td>Nr. ture</td><td>Val lei</td></tr>");
//
//          client.println("<tr><th>Masinuta 1</th><td>15</td><td>");
//          client.println(employee_table[1][1][3]); //masina1 angajat1 luna3
//          client.println("</td><td>");
//          client.println(15*employee_table[1][1][3]); //15lei * nr ture
//          client.println("</td><td>");
//          client.println(employee_table[1][2][3]); //masina1 angajat2 luna3
//          client.println("</td><td>");
//          client.println(15*employee_table[1][2][3]); //15lei * nr ture
//          client.println("</td><td>");
//          client.println(employee_table[1][3][3]); //masina1 angajat3 luna3
//          client.println("</td><td>");
//          client.println(15*employee_table[1][3][3]); //15lei * nr ture
//          client.println("</td></tr>");
//
//          client.println("<tr><th>Masinuta 2</th><td>15</td><td>");
//          client.println(employee_table[2][1][3]); //masina2 angajat1 luna3
//          client.println("</td><td>");
//          client.println(15*employee_table[2][1][3]); //15lei * nr ture
//          client.println("</td><td>");
//          client.println(employee_table[2][2][3]); //masina2 angajat2 luna3
//          client.println("</td><td>");
//          client.println(15*employee_table[2][2][3]); //15lei * nr ture
//          client.println("</td><td>");
//          client.println(employee_table[2][3][3]); //masina2 angajat3 luna3
//          client.println("</td><td>");
//          client.println(15*employee_table[2][3][3]); //15lei * nr ture
//          client.println("</td></tr>");
//
//          client.println("<tr><th>Masinuta 3</th><td>10</td><td>");
//          client.println(employee_table[3][1][3]); //masina3 angajat1 luna3
//          client.println("</td><td>");
//          client.println(10*employee_table[3][1][3]); //10lei * nr ture
//          client.println("</td><td>");
//          client.println(employee_table[3][2][3]); //masina3 angajat2 luna3
//          client.println("</td><td>");
//          client.println(10*employee_table[3][2][3]); //10lei * nr ture
//          client.println("</td><td>");
//          client.println(employee_table[3][3][3]); //masina3 angajat3 luna3
//          client.println("</td><td>");
//          client.println(10*employee_table[3][3][3]); //10lei * nr ture
//          client.println("</td></tr>");
//
//          client.println("<tr><th>Masinuta 4</th><td>15</td><td>");
//          client.println(employee_table[4][1][3]); //masina4 angajat1 luna3
//          client.println("</td><td>");
//          client.println(15*employee_table[4][1][3]); //15lei * nr ture
//          client.println("</td><td>");
//          client.println(employee_table[4][2][3]); //masina4 angajat2 luna3
//          client.println("</td><td>");
//          client.println(15*employee_table[4][2][3]); //15lei * nr ture
//          client.println("</td><td>");
//          client.println(employee_table[4][3][3]); //masina4 angajat3 luna3
//          client.println("</td><td>");
//          client.println(15*employee_table[4][3][3]); //10lei * nr ture
//          client.println("</td></tr>");
//
//          client.println("<tr><th>Masinuta 5</th><td>10</td><td>");
//          client.println(employee_table[5][1][3]); //masina5 angajat1 luna3
//          client.println("</td><td>");
//          client.println(10*employee_table[5][1][3]); //10lei * nr ture
//          client.println("</td><td>");
//          client.println(employee_table[5][2][3]); //masina5 angajat2 luna3
//          client.println("</td><td>");
//          client.println(10*employee_table[5][2][3]); //10lei * nr ture
//          client.println("</td><td>");
//          client.println(employee_table[5][3][3]); //masina5 angajat3 luna3
//          client.println("</td><td>");
//          client.println(10*employee_table[5][3][3]); //10lei * nr ture
//          client.println("</td></tr>");
//          
//          client.println("<tr><th colspan=2></th><th>TOTAL</th><td>");
//          client.println(15*employee_table[1][1][3] + 15*employee_table[2][1][3] + 10*employee_table[3][1][3] + 15*employee_table[4][1][3] + 10*employee_table[5][1][3]); //total angajat1 (masina1+...+masina5)
//          client.println("</td><th>TOTAL</th><td>");
//          client.println(15*employee_table[1][2][3] + 15*employee_table[2][2][3] + 10*employee_table[3][2][3] + 15*employee_table[4][2][3] + 10*employee_table[5][2][3]); //total angajat2 (masina1+...+masina5)
//          client.println("</td><th>TOTAL</th><td>");
//          client.println(15*employee_table[1][3][3] + 15*employee_table[2][3][3] + 10*employee_table[3][3][3] + 15*employee_table[4][3][3] + 10*employee_table[5][3][3]); //total angajat3 (masina1+...+masina5)
//          client.println("</td></tr>");
//
//
//          client.print("</table>");



//          for (int i = 1; i <= nr_cars; i++)
//          {
//            client.println("<tr><td>Masinuta ");
//            client.println(i);
//            client.println("</td><td> ");
//            client.println(records[i]);
//            client.println("  </td></tr>");
//          }
//          client.print("</table>");

        }
        else//if worng password terminate with message
        {

          Serial.println("Response Sent to Client: A HTML Page");
          client.println("HTTP/1.1 200 OK");

          client.println("Content-Type: text/html\n");
          client.println("Sorry you've been disconnected");


        }

        break;
      }
    }
    delay(5);
    client.stop();
    Serial.println("Client is disconnected");
  }
  while (radio.available()) // check if car is sending message
  {

    int msg[message_length];
    radio.read(msg, sizeof(int)*message_length);
    if (msg[0] == 0)//msg={operation,car_id,total_laps}
    {
      //records[msg[1]] = msg[2];                          //total laps of a car, done often automaticaly
    }
    if (msg[0] == 1)               //msg={operation,car_id,employee_id,nr_laps}
    {
     
      employee_table.work[msg[1]-1][msg[2]-1][month_table[get_month()]][get_day()-1] += msg[3];

     // Serial.print(employee_table.work[msg[2]][msg[1]][get_month()%7][get_day()]);
      create_backup();
    }






  }

  delay(2000); // Wait for 2 seconds before starting to listen again
}

