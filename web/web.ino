#include <Ethernet.h>
#include<SPI.h>
#include <EEPROM.h>
#include <DS3231.h>
#include "RF24.h"
#define nr_cars 5
#define message_length 4
#define nr_employees 3
#define SDA_CLOCK A9
#define SCL_CLOCK A10

int employee_table[nr_cars + 1][nr_employees + 1][13] ;       ///////////STARTS AT 1 !!!!!!!!!!!!!!
byte mac[] = {0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02};
EthernetServer server = EthernetServer(12345);
RF24 radio(9, 53);
byte addresses[][6] = {"ABCDE"};
//IPAddress ip(192, 168, 1, 177);
DS3231  rtc(SDA_CLOCK, SCL_CLOCK);//THE CLOCK OBJECT
char paskey[20] = "12345678";
struct backup_table
{
  int work[nr_cars + 1][nr_employees + 1][13];
};

void restore_table()
{
  struct backup_table table;
  EEPROM.get(0, table);
  for (int i = 1; i <= nr_cars; i++)
  {
    for (int j = 1; j <= nr_employees; j++)
    {
      for (int k = 1; k <= 12; k++)
      {
        employee_table[i][j][k] =  table.work[i][j][k];
      }
    }
  }
}

void setup() {
  radio.begin();
  rtc.begin();
  restore_table();
  Serial.begin(115200);
  boolean receiving = false;

  if (Ethernet.begin(mac) == 0) {    Serial.println("Failed to Configure");
    return;
  }
  else {
    Serial.print("LocalIP address of the arduino:");
    Serial.println(Ethernet.localIP());
  }

  server.begin();
  Serial.println("Server Started");
  radio.openReadingPipe(1, addresses[0]);
  radio.startListening();
  
}


void reset_records()
{
  struct backup_table table;
  for (int i = 1; i <= nr_cars; i++)
  {
    for (int j = 1; j <= nr_employees; j++)
    {

      for (int k = 1; k <= 12; k++)
      {

        table.work[i][j][k] = 0;
      }
    }
  }
  EEPROM.put(0, table);
  
}

int get_month()
{
  char month[100];


  if (strcmp(rtc.getMonthStr(), "January") == 0)

    return 1;
  else if (strcmp(rtc.getMonthStr(), "February") == 0)


    return 2;

  else if (strcmp(rtc.getMonthStr(), "March") == 0)

    return 3;

  else if (strcmp(rtc.getMonthStr(), "April") == 0)

    return 4;
  if (strcmp(rtc.getMonthStr(), "May") == 0)


    return 5;
  if (strcmp(rtc.getMonthStr(), "June") == 0)

    return 6;
  if (strcmp(rtc.getMonthStr(), "July") == 0)

    return 7;
  if (strcmp(rtc.getMonthStr(), "August") == 0)

    return 8;
  if (strcmp(rtc.getMonthStr(), "September") == 0)

    return 9;
  if (strcmp(rtc.getMonthStr(), "October") == 0)

    return 10;
  if (strcmp(rtc.getMonthStr(), "November") == 0)

    return 11;
  if (strcmp(rtc.getMonthStr(), "December") == 0)

    return 12;


}
void create_backup()
{
  struct backup_table table;
  for (int i = 1; i <= nr_cars; i++)
  {
    for (int j = 1; j <= nr_employees; j++)
    {

      for (int k = 1; k <= 12; k++)
      {

        table.work[i][j][k] = employee_table[i][j][k];
      }
    }
  }
  EEPROM.put(0, table);

}



int records[nr_cars + 1];
void print_work()
{


  for (int i = 1; i <= 12; i++)
  {
    Serial.print("Month ");
    Serial.println(i);

    for (int j = 1; j <=nr_employees ; j++)
    {
      Serial.print("       ");
      Serial.print("Employee ");
      Serial.println(j);
      for (int k = 1; k <= nr_cars; k++)
      {
        Serial.print("               ");
        Serial.print("Car ");
        Serial.print(k);
        Serial.print(": ");
        Serial.println(  employee_table[k][j][i]);
      }
    }
  }
}


////////////////////////////////////////////////MAIN
void loop() {
  //Serial.println(rtc.getMonthStr());
  if (Serial.available())
  {
    char c;
    c = Serial.read();
    if (c == 'P')
    {
      print_work();

    }
    else
    if(c =='R')
    {
      reset_records();
      restore_table();
    }
  }
  //Serial.println("Waiting for a client to connect");
  EthernetClient client = server.available();

  if (client) {



    Serial.print("Connected to a client : ");

    while (client.available()) {



      if (client.connected()) {
        char c = 'y';
        Serial.print("starting to print");
        char buf[100] = "";
        int reading = 0;
        int index = 0;
        while ((c =  client.read()) != ' ')
        {


        }


        while ( (c =  client.read()) != ' ')
        {

          if (reading)
          {
            buf[index] = c;
            index++;

          }
          if ((c == '?') && (!reading))
          {

            reading = 1;
          }


          Serial.println();
        }
        if (strcmp(paskey, buf) == 0)
        {





          Serial.println("Response Sent to Client: A HTML Page");
          client.println("HTTP/1.1 200 OK");

          client.println("Content-Type: text/html\n");
          client.println("<table  border=1 cellpadding=5 cellspacing=5>");
          for (int i = 1; i <= nr_cars; i++)
          {
            client.println("<tr><td>Masinuta ");
            client.println(i);
            client.println("</td><td> ");
            client.println(records[i]);
            client.println("  </td></tr>");
          }
          client.print("</table>");

        }
        else
        {

          Serial.println("Response Sent to Client: A HTML Page");
          client.println("HTTP/1.1 200 OK");

          client.println("Content-Type: text/html\n");
          client.println("Sorry you've been disconnected");


        }

        break;
      }
    }
    delay(5);
    client.stop();
    Serial.println("Client is disconnected");
  }
  while (radio.available()) // blocking
  {

    int msg[message_length];
    radio.read(msg, sizeof(int)*message_length);
    if (msg[0] == 0)
    {
      records[msg[1]] = msg[2];
    }
    if (msg[0] == 1)
    {
      //rtc.getMonthStr();
      employee_table[msg[1]][msg[2]][get_month()] += msg[3];

      Serial.print(employee_table[msg[2]][msg[1]][get_month()]);
      create_backup();
    }






  }

  delay(2000); // Wait for 2 seconds before starting to listen again
}

