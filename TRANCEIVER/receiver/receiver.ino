//#include <nRF24L01.h>
#include <RF24.h>
//#include <RF24_config.h>
#include <SPI.h>
#include <MFRC522.h>
/*
 * 
This is the corresponding sketch to the 'basicSend' sketch.
the nrf24l01 will listen for numbers 0-255, and light the red LED
whenever a number in the sequence is missed.  Otherwise,
it lights the green LED
*/
byte msg[1];

RF24 radio(8,9);
const uint64_t pipe = 0xE8E8F0F0E1LL;
int red = 3;
int green = 5;
int redNeg = 4;
int greenNeg = 6;
int lastmsg = 1;
 byte addresses[][6] = {"ABCDE"};
void setup(void){
  
  Serial.begin(9600);
   SPI.begin();
  radio.begin();
  radio.openReadingPipe(1, addresses[0]);
    // radio.setDataRate( RF24_250KBPS );
      
  radio.startListening();
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(redNeg, OUTPUT);
  pinMode(greenNeg, OUTPUT);
  digitalWrite(greenNeg, LOW);
  digitalWrite(redNeg, LOW);
  Serial.println("running");
}
 
void loop(void){
 
  if (radio.available()){
    bool done = false;  
  // Serial.println("running");

    radio.read(msg, 1); 
    
      Serial.println(msg[0]);
     
      
     
    }
    
}
